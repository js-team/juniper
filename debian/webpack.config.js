'use strict';

var fs = require('fs');
var path = require('path');
var webpack = require('webpack');

var config = {

  target: 'web',
  resolve: {
    modules: ['/usr/lib/nodejs', '.'],
  },
  resolveLoader: {
    modules: ['/usr/lib/nodejs'],
  },
  node: {
    fs: 'empty'
  },
  output: {
    libraryTarget: 'umd'
  },
  module: { rules: [ { use: [ 'babel-loader' ] } ] }
}

module.exports = config;

